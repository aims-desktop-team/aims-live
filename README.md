# aims-live package

This package is part of AIMS Desktop.

It provides:

 - The dependencies in order to boot up a system from live media such
   as a DVD-RW disc or a USB disk.
 - Settings for Calamares, the installer framework used to install
   AIMS Desktop.
 - A list of conflicts that are used during the installation process
   in order to remove unwanted or very large packages.

